import { Link } from "@solidjs/router";
import { For, createResource, createSignal, onMount } from "solid-js";
import { socket } from "../socket";

async function fetchOnlineGames() {
  return (await fetch("http://localhost:4000/")).json();
}

export default function Home() {
  const [games, { refetch, mutate }] = createResource(fetchOnlineGames);

  let input: HTMLInputElement;

  return (
    <div class="pt-4 pl-8 flex flex-col space-y-4">
      <h1 class="text text-lg">Online Games</h1>
      <ul class="flex flex-col space-y-4">
        <For each={games()}>
          {(item) => (
            <li>
              <Link href={"/games/" + item.id}>
                <button class="btn btn-lg btn-primary">{item.name}</button>
              </Link>
            </li>
          )}
        </For>
      </ul>
      <div class="flex flex-row space-x-4">
        <form class="flex flex-row space-x-2" onSubmit={(evt) => createGame(evt)}>
          <input
            class="input input-primary"
            type="text"
            name="newGame"
            id="newGame"
            ref={input}
            placeholder="name your game"
          />
          <input class="btn btn-accent" type="submit" value="Create Game" />
        </form>
        <button class="btn btn-secondary" onClick={() => refetch()}>
          Refresh Games
        </button>
        <button class="btn" onClick={() => clearGames()}>
          Clear Games
        </button>
      </div>
    </div>
  );

  async function clearGames() {
    alert(
      "this will shutdown all game servers and clear all entries in the db, are you sure ?"
    );
    await fetch("http://localhost:4000/games", {
      method: "DELETE",
    }).then((resp) => {
      console.log(resp);
      mutate([]);
    });
  }

  async function createGame(event) {
    event.preventDefault();
    const gameName = event.target.elements.newGame.value;
    await fetch("http://localhost:4000/games", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name: gameName }),
    }).then((resp) => {
      input.value = "";
      refetch();
    });
  }
}
