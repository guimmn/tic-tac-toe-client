import { useLocation } from "@solidjs/router";
import {
  For,
  Match,
  Show,
  createEffect,
  createSignal,
  onCleanup,
  onMount,
} from "solid-js";
import { createStore } from "solid-js/store";
import { socket } from "../socket";
import { Channel } from "phoenix";

export default function Games() {
  const [id, setId] = createSignal();
  const [figure, setFigure] = createSignal("");
  const [channel, setChannel] = createSignal<Channel>();

  //represents game state
  const [gameState, setGameState] = createStore({
    state: {
      name: "",
      board: [null, null, null, null, null, null, null, null, null],
      winner: null,
      finished: false,
      this_turn: "x"
    },
  });

  //init
  onMount(() => {
    const gameId = useLocation().pathname.split("/")[2];

    setId(gameId);

    //setup channel
    const channel = socket.channel("game:" + gameId);

    //join channel
    channel
      .join()
      //joined channel with success
      .receive("ok", (resp) => {
        console.log(
          "successfully connected to channel with id " + gameId,
          resp
        );

        //register in game
        console.log("joining game");
        channel
          //name is not being used, at the moment at least
          .push("join", { id: gameId, name: "placeholder" })
          .receive("ok", (resp) => setFigure(resp.figure))

          //game is full
          .receive("error", (resp) => {
            alert("game is full :(");
            //window.location.replace("/");
          });
      })
      .receive("error", (resp) => {
        console.log("couldn't join game", resp);
        channel.leave();
      });
    //to access on cleanup
    setChannel(channel);

    //SETUP CALLBACKS --
    //move
    channel.on("move", (state) => setGameState(state));
    //join
    channel.on("join", (state) => {
      setGameState(state);
      console.log(state);
    });
    //quit
    channel.on("quit", (state) => setGameState(state));
    //--
  });

  onCleanup(() => {
    console.log("leaving channel");
    channel().push("quit", { id: id(), figure: figure() });
    channel().leave();
  });

  return (
    <div class="p-36">
      <h1 class="text text-5xl">{gameState.state!.name}</h1>
      <Show when={gameState.state.winner == figure()}>
        <h1 class="text text-3xl">YOU WON</h1>
      </Show>
      <Show
        when={
          gameState.state.winner == null && gameState.state.finished == true
        }
      >
        <h1 class="text text-3xl">TIE</h1>
      </Show>
      <div class="flex flex-row space-x-20 w-full">
        <div class="grid h-20 flex-grow card bg-base-300 rounded-box place-items-center">
        <Show when={!gameState.state.finished && gameState.state.this_turn == figure()}>
        <h2 class="text text-xl">YOUR TURN</h2>
        </Show>
          <div id="me">
            <h3 class="text text-xl text-accent">ME: {figure()}</h3>
          </div>
        </div>
        <div class="divider divider-horizontal"></div>
        <div
          id="game"
          class="flex flex-col justify-center items-center space-y-4 w-1/2"
        >
          <div class="flex space-x-4 w-full justify-center items-center">
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 0)}
            >
              {gameState.state.board[0]}
            </button>
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 1)}
            >
              {gameState.state.board[1]}
            </button>
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 2)}
            >
              {gameState.state.board[2]}
            </button>
          </div>
          <div class="flex space-x-4 w-full justify-center items-center">
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 3)}
            >
              {gameState.state.board[3]}
            </button>
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 4)}
            >
              {gameState.state.board[4]}
            </button>
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 5)}
            >
              {gameState.state.board[5]}
            </button>
          </div>
          <div class="flex space-x-4 w-full justify-center items-center">
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 6)}
            >
              {gameState.state.board[6]}
            </button>
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 7)}
            >
              {gameState.state.board[7]}
            </button>
            <button
              disabled={gameState.state.finished}
              class="btn btn-primary btn-lg w-20 h-20"
              onClick={() => move(figure(), 8)}
            >
              {gameState.state.board[8]}
            </button>
          </div>
        </div>
        <div class="divider divider-horizontal"></div>
        <div class="grid h-20 flex-grow card bg-base-300 rounded-box place-items-center">
          <div id="opponent">
            <h3 class="text text-xl">
              OPPONENT: {figure() == "x" ? "o" : "x"}
            </h3>
          </div>
        </div>
      </div>
    </div>
  );

  function move(figure: string, position: number) {
    channel().push("move", { id: id(), figure: figure, position: position });
  }

  function quit(figure: string) {
    channel().push("quit", { id: id(), figure: figure });
  }
}
