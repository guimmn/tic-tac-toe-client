import { lazy } from 'solid-js';
import type { RouteDefinition } from '@solidjs/router';

import Home from './pages/home';
import Games from './pages/games';

export const routes: RouteDefinition[] = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/games/:id',
    component: Games,
  },
  {
    path: '**',
    component: lazy(() => import('./errors/404')),
  },
];
